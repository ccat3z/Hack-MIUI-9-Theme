package cc.c0ldcat.hackmiui9theme;

import android.content.Context;
import de.robv.android.xposed.*;
import de.robv.android.xposed.callbacks.XC_LoadPackage;

import java.io.File;

public class Hooker implements IXposedHookLoadPackage, IXposedHookZygoteInit {
    @Override
    public void handleLoadPackage(XC_LoadPackage.LoadPackageParam loadPackageParam) throws Throwable {
        if (loadPackageParam.packageName.equals("miui.drm")
                || loadPackageParam.packageName.equals("com.miui.system")
                || loadPackageParam.packageName.equals("miui.system")) {
            initZygote(null);
        }

        if (loadPackageParam.packageName.equals("com.android.thememanager")) {
            XposedBridge.log("hook miui theme store checking rights method");
            // getProductPrice()
            // from string 购买
            XposedHelpers.findAndHookMethod("com.android.thememanager.e.s",
                    loadPackageParam.classLoader,
                    "getProductPrice",
                    XC_MethodReplacement.returnConstant(0));

            // isLegal()
            // 1.2.2 string resource_get_auth_not_official -> DownloadRightsTask(AsyncTask).onPostExecute() ->
            // switch(-getcom-android-thememanager-controller-online-DrmService$GettingAuthorizeCodeSwitchesValues) ->
            // ThemeOperationHandler.this.checkResourceRights() -> isLegal()
            XposedHelpers.findAndHookMethod("com.android.thememanager.util.dv",
                    loadPackageParam.classLoader,
                    "C",
                    XC_MethodReplacement.returnConstant(true));

            // isTrialable()
            // 1.2.2 class of isLegal()
            XposedHelpers.findAndHookMethod("com.android.thememanager.util.dv",
                    loadPackageParam.classLoader,
                    "v",
                    XC_MethodReplacement.returnConstant(true));

            // isAuthorizedResource()
            // 1.2.2 class of isLegal()
            XposedHelpers.findAndHookMethod("com.android.thememanager.util.dv",
                    loadPackageParam.classLoader,
                    "k",
                    XC_MethodReplacement.returnConstant(true));

            // isPermanentRights()
            // 1.2.2 class of isLegal()
            XposedHelpers.findAndHookMethod("com.android.thememanager.util.dv",
                    loadPackageParam.classLoader,
                    "x",
                    XC_MethodReplacement.returnConstant(true));
        }
    }

    @Override
    public void initZygote(StartupParam startupParam) throws Throwable {
        XposedBridge.log("hook miui theme validating receiver");
        XposedHelpers.findAndHookMethod(Class.forName("miui.drm.DrmManager"), "isLegal",
                Context.class, File.class, File.class,
                XC_MethodReplacement.returnConstant(getDrmResultSUCCESS()));
        XposedHelpers.findAndHookMethod(Class.forName("miui.drm.DrmManager"), "isLegal",
                Context.class, String.class, File.class,
                XC_MethodReplacement.returnConstant(getDrmResultSUCCESS()));
        XposedHelpers.findAndHookMethod(Class.forName("miui.drm.DrmManager"), "isPermanentRights",
                File.class, XC_MethodReplacement.returnConstant(true));
    }

    private static Object getDrmResultSUCCESS() {
        try {
            Class<Enum> drmSuccess = (Class<Enum>) Class.forName("miui.drm.DrmManager$DrmResult");
            if (drmSuccess != null) {
                return Enum.valueOf(drmSuccess, "DRM_SUCCESS");
            }
        } catch (Throwable localString4) {
            XposedBridge.log(localString4);
        }
        return null;
    }
}
